<?php
/**
 * programming_printer block caps.
 *
 * @package   block_programming_printer
 * 
 */

defined('MOODLE_INTERNAL') || die();

$capabilities = array(

    // 'block/programming_printer:myaddinstance' => array(
    //     'captype' => 'write',
    //     'contextlevel' => CONTEXT_SYSTEM,
    //     'archetypes' => array(
    //         'user' => CAP_ALLOW
    //     ),

    //     'clonepermissionsfrom' => 'moodle/my:manageblocks'
    // ),

    'block/programming_printer:addinstance' => array(
        'riskbitmask' => RISK_SPAM | RISK_XSS,

        'captype' => 'write',
        'contextlevel' => CONTEXT_BLOCK,
        'archetypes' => array(
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW
        ),

        'clonepermissionsfrom' => 'moodle/site:manageblocks'
    ),

    // 'block/programming_printer:manageownprinters' => array(

    //     'captype' => 'write',
    //     'contextlevel' => CONTEXT_BLOCK,
    //     'archetypes' => array(
    //         'teacher' => CAP_ALLOW,
    //         'editingteacher' => CAP_ALLOW,
    //         'manager' => CAP_ALLOW
    //     )
    // ),

    'block/programming_printer:manageanyprinters' => array(

        'riskbitmask' => RISK_SPAM,

        'captype' => 'write',
        'contextlevel' => CONTEXT_BLOCK,
        'archetypes' => array(
            'manager' => CAP_ALLOW
        )
    )
);
