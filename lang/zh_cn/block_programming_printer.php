<?php
/**
 * programming_printer block.
 *
 * @package   block_programming_printer
 * 
 */
$string['addnewprinter'] = '添加打印机';
$string['printerdeleted'] = '打印机已删除';
$string['deleteprinterconfirm'] = '是否确定删除打印机信息？';
$string['descenableprinting'] = '打开或关闭网站打印功能。';
$string['disableprinting'] = '该网站打印功能已关闭。';
$string['editaprinter'] = '编辑打印机信息';
$string['enableprinting'] = '允许打印';
$string['id'] = 'ID';
$string['manageprinters'] = '管理打印机';
$string['nousableprinter'] = '没有可用的打印机！';
$string['printcontent'] = '打印内容';
$string['printcontentdefult'] = '将需要打印的内容粘贴到该区域。';
$string['printername'] = '打印机名称';
$string['printer'] = '打印机';
$string['printerid'] = '打印机编号';
$string['printerip'] = '打印服务器IP地址';
$string['printersaddedit'] = '添加/编辑打印机信息';
$string['printerdeleted'] = '打印机删除成功。';
$string['printingfail'] = '打印失败！';
$string['printingsucceed'] = '打印成功！';
$string['printingsubmit'] = '提交';
$string['programming_printer:addinstance'] = '添加打印机模块';
$string['programming_printer:myaddinstance'] = '添加打印机模块';
$string['pluginname'] = '打印';
$string['strrequired'] = '打印内容不能为空！';
$string['subnet'] = '可访问该打印机的子网';
$string['usable'] = '是否可用';
$string['username'] = '用户名';
$string['manageprinters'] = '管理打印机';
