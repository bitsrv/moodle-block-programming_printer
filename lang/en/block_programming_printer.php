<?php
/**
 * programming_printer block.
 *
 * @package   block_programming_printer
 * 
 */

$string['addnewprinter'] = 'Add New Printer';
$string['blockstring'] = 'Block string';
$string['printerdeleted'] = 'The printer has deleted!';
$string['deleteprinterconfirm'] = 'Are you sure you want to delete this printer?';
$string['descconfig'] = 'Description of the config section';
$string['descenableprinting'] = 'enable or disable printing function.';
$string['disableprinting'] = 'This site is not allowed to print via remote printer!';
$string['editaprinter'] = 'Edit Printer';
$string['enableprinting'] = 'Enable Printing';
$string['headerconfig'] = 'Config section header';
$string['id'] = 'ID';
$string['manageprinters'] = 'Manage Printers';
$string['nousableprinter'] = 'There is not a printer can be used!';
$string['printcontent'] = 'Content';
$string['printcontentdefult'] = 'Paste your code';
$string['printername'] = 'Printer Name';
$string['printer'] = 'Printer';
$string['printerid'] = 'Printer ID';
$string['printerip'] = 'Printe Server IP';
$string['printersaddedit'] = 'Add/Edit Printers';
$string['printerdeleted'] = 'Printer Deleted';
$string['printingfail'] = 'Printing Fail';
$string['printingsucceed'] = 'Printing Succeed!';
$string['printingsubmit'] = 'Submit';
$string['programming_printer:addinstance'] = 'Add a programming_printer block';
$string['programming_printer:myaddinstance'] = 'Add a programming_printer block to my moodle';
$string['pluginname'] = 'Programming Printer';
$string['strrequired'] = 'You must add your content!';
$string['subnet'] = 'Subnet';
$string['usable'] = 'Usable';
$string['username'] = 'User Name';
$string['manageprinters'] = 'Manage Printers';
