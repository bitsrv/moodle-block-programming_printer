<?php
/**
 * programming_printer block caps.
 *
 * @package   block_programming_printer
 * 
 */
defined('MOODLE_INTERNAL') || die();

$settings->add(new admin_setting_heading('sampleheader',
                                         get_string('headerconfig', 'block_programming_printer'),
                                         get_string('descconfig', 'block_programming_printer')));

$settings->add(new admin_setting_configcheckbox('programming_printer/enableprinting',
                                                get_string('enableprinting', 'block_programming_printer'),
                                                get_string('descenableprinting', 'block_programming_printer'),
                                                '0'));
