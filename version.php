<?php
/**
 * programming_printer block caps.
 *
 * @package   block_programming_printer
 * 
 */
defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2013011300;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2012112900;        // Requires this Moodle version
$plugin->component = 'block_programming_printer'; // Full name of the plugin (used for diagnostics)
$plugin->cron = 300;
