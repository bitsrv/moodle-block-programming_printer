<?php
/**
 * programming_printer block.
 *
 * @package   block_programming_printer
 * 
 */

defined('MOODLE_INTERNAL') || die();

class block_programming_printer extends block_base {

    function init() {
        $this->title = get_string('pluginname', 'block_programming_printer');
    }

    function get_content() {
        global $CFG, $OUTPUT, $DB;

        if ($this->content !== null) {
            return $this->content;
        }
        // initalise block content object
        $this->content = new stdClass;
        $this->content->items = array();
        $this->content->icons = array();
        $this->content->text   = "";
        $this->content->footer = "";

        if (empty($this->instance) or !isloggedin() or isguestuser() ) {
            return $this->content;
        }
        // user/index.php expect course context, so get one if page has module context.
        $currentcontext = $this->page->context->get_course_context(false);
        if (empty($currentcontext)) {
            return $this->content;
        }

        // 判定该网站是否开启远程打印服务
        $config = get_config('programming_printer','enableprinting');
        if (!$config) {
            $this->content->text .= get_string('disableprinting', 'block_programming_printer');
            return $this->content;
        }

        // 读取同局域网内的可用的打印机
        $printers = $DB->get_records('block_programming_printer',array("usable"=>"1"));
        if (empty($printers)) { // 无可用打印机
            $this->content->text .= get_string('nousableprinter', 'block_programming_printer');
            return $this->content;
        }
 
        $remoteaddr = getremoteaddr();
        // echo "hello remoteaddr = ".$remoteaddr;
        // 获取ke yong de 打印机名称，并以链接方式显示在block中
        $i = 0;
        $output = "";
        $output .= '<ul class="list no-overflow">'."\n";
        foreach ($printers as $printer) {
            if (address_in_subnet($remoteaddr, $printer->subnet)) {
                // $output .= "i = ".$i."  remoteaddr = ".$remoteaddr;
                $output .= $this->get_usable_printer($printer);
                $i = $i+1;
            }
        }
        if ($i == 0) { // 无可用打印机
            $output .= get_string('nousableprinter', 'block_programming_printer');
        }
//---------测试-------------------------------------------
        // foreach ($printers as $printer) {
        //     $printerlist = "";
        //     $printerlist .= $this->get_usable_printer($printer);
        //     $output .= $printerlist;
        // }
//---------测试-------------------------------------------
        $output .= '</ul>';
        $this->content->text .= $output;
        return $this->content;
    }


    /**
     * 读取打印机名称，并生成HTML
     * @param $printer 打印机信息 
     */
    function get_usable_printer ($printer){
        global $CFG,$USER;
        // TODO 确定是否需要sesskey；
        // $link = $CFG->wwwroot . '/blocks/programming_printer/printing.php?pid=' . $printer->id . '&userid='.$USER->id.'&sesskey='.sesskey();
        $link = $CFG->wwwroot . '/blocks/programming_printer/printing.php?pid=' . $printer->id . '&userid='.$USER->id;//  . '&courseid=' . $this->page->course->id;
        $output = "";
        $description = $printer->printername;

        $formatoptions = new stdClass();
        $formatoptions->para = false;
        $description = format_text($description, FORMAT_HTML, $formatoptions, $this->page->course->id);
        $description = break_up_long_words($description, 30);

        $output = html_writer::start_tag('li');
        $output.= html_writer::start_tag('div',array('class'=>'link'));

        $output.= html_writer::link($link, s($description), array('onclick'=>'this.target="_blank"'));

        $output.= html_writer::end_tag('div');
        $output.= html_writer::end_tag('li'); 

        return $output;   
    }
    // my moodle can only have SITEID and it's redundant here, so take it away
    public function applicable_formats() {
        return array('all' => false,
                     'site' => true,
                     'site-index' => true,
                     'course-view' => true, 
                     'course-view-social' => false,
                     'mod' => true, 
                     'mod-quiz' => false);
    }

    public function instance_allow_multiple() {
          return true;
    }

    function has_config() {return true;}

    public function cron() {
            mtrace( "Hey, my cron script is running" );
            return true;
    }
}
