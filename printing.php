<?php
/**
 * programming_printer block caps.
 *
 * @package   block_programming_printer
 * @author hedaqing <daqing_he@163.com>
 */
require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir .'/simplepie/moodle_simplepie.php');
include_once('../../lib/tablelib.php');

//
class printing_form extends moodleform {

    protected $username = '';
    protected $printername = '';
    protected $actionurl = '';

    function __construct($actionurl, $username, $printername) {
        $this->username = $username;
        $this->printername = $printername;
        $this->actionurl = $actionurl;
        parent::moodleform($actionurl);
    }

    function definition() {
        $mform =& $this->_form;

        $mform->addElement('static', 'printername', get_string('printername','block_programming_printer').': ', $this->printername);
        $mform->addElement('static', 'username', get_string('username','block_programming_printer').': ', $this->username);

        $mform->addElement('textarea', 'printcontent', get_string('printcontent', 'block_programming_printer'),
                array('rows' => 20, 'cols' => 120));
        $mform->addRule('printcontent', get_string('strrequired', 'block_programming_printer'), 'required', null, 'client');
        // $mform->setDefault('printcontent', get_string('printcontentdefult', 'block_programming_printer')); // 默认值
        $mform->setType('printcontent', PARAM_RAW);
        // $mform->addHelpButton('printcontent', 'sitedesc', 'hub');

        $this->add_action_buttons(true, get_string('printingsubmit', 'block_programming_printer'));
    }
}

// 判断是否登录，如未登录，则跳转至登录界面
require_login(0,false);
if (isguestuser()) {
    // Login as real user!
    $SESSION->wantsurl = (string)new moodle_url('/index.php');
    redirect(get_login_url());
}

$pid = optional_param('pid', 0, PARAM_INT); // 打印机ID
$userid = optional_param('userid', 0, PARAM_INT); // 用户ID
$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
$course = optional_param('course', 0, RARAM_INT);

$url = '/blocks/programming_printer/printing.php';

// // 确定是否需要sesskey
// // if ($pid && confirm_sesskey() && $userid && !$printtask) {
// if ($pid && $userid && !$printtask) { // 进入打印编辑页面

// 判定该网站是否开启远程打印服务
$config = get_config('programming_printer','enableprinting');
if (!$config) {
    redirect($CFG->wwwroot, get_string('disableprinting', 'block_programming_printer'));
}

// 获取打印机和用户信息 
$printer = $DB->get_record('block_programming_printer', array('id'=>$pid) );
// printer can not used
if (!$printer->usable) {
    redirect($CFG->wwwroot, "The Printer can NOT be used!");
}

$remoteaddr = getremoteaddr();
if (!address_in_subnet($remoteaddr, $printer->subnet)) {
    redirect($CFG->wwwroot, "The Printer can NOT be used!");
}

$user = $DB->get_record('user', array('id'=>$userid) );

$urlparams = array('pid' => $pid, 'userid'=>$userid);
if (!$returnurl) {
    $urlparams['returnurl'] = $url.'?pid=' . $printer->id . '&userid='.$USER->id;
}

$baseurl = new moodle_url($url, $urlparams);

$PAGE->set_url('/blocks/programming_printer/printing.php', $urlparams);
$mform = new printing_form($PAGE->url, $user->username, $printer->printername);


if ($mform->is_cancelled()) {
    redirect($baseurl);

} else if ($data = $mform->get_data()) {
    $userfullname = $USER->lastname.$USER->username;
    $d = $CFG->dataroot.'/temp/programming';
    if (!is_dir($d)) {
        if (file_exists($d)) {
            unlink($d);
        }
        mkdir($d);
    }
    $srcname = tempnam($d,$userfullname.'_');
    $destname = tempnam($d, $userfullname.'_');

    $f = fopen($srcname, 'w');
    fwrite($f, $data->printcontent);
    fclose($f);
    // // print_r(file($srcname));
    // system("file -i \"$srcname\" > /tmp/b");
    // // system("iconv -f utf-8 -t zh_CN.utf8 $srcname > $srcname");
    // // putenv('LC_ALL=en_US.UTF-8');
    putenv('LC_ALL=zh_CN.UTF-8');
    // header('Content-Type: application/pdf');
    // header('Content-Disposition: attachment; filename="source.pdf"');
    // passthru("/usr/bin/u2ps -o /dev/stdout -t \"$userfullname\" --gpfamily=\"Monospace\" \"$srcname\" 2>/dev/null | /usr/bin/ps2pdf - -");

    // // unlink($srcname);
    // unlink($destname);
    // system("/usr/bin/u2ps -o \"$destname\" -t \"$userfullname\" --gpfamily=\"Monospace\" \"$srcname\" 2>/dev/null");
    $srcfilename = basename($srcname);
    $destfilename = basename($destname);
    system("cd \"$d\" ; paps --header --lpi=8 --font=\"Monospace 10\" \"$srcfilename\">\"$destfilename\"");
    $destprt = $printer->printerid;
    $destip = $printer->printerip;

    if ($destprt) {
        $out = system("/usr/bin/lp -d \"$destprt\" -h \"$destip\" \"$destname\"");
        $result = get_string('printingsucceed', 'block_programming_printer');
    } else {
        $result = get_string('printingfail', 'block_programming_printer');
    }

    unlink($srcname);
    unlink($destname);
    redirect($baseurl, $result);
} else {

    $PAGE->set_title("printing");
    $PAGE->set_heading(get_string('pluginname', 'block_programming_printer'));

    $PAGE->navbar->add('blocks');
    $PAGE->navbar->add('Printer');
    $PAGE->navbar->add('Printing');
    echo $OUTPUT->header();
    $mform->display();

    echo $OUTPUT->footer();
}
