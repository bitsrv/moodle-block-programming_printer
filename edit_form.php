<?php

/**
 * programming_printer block.
 *
 * @package   block_programming_printer
 * 
 */

class block_programming_printer_edit_form extends block_edit_form {


    protected function specific_definition($mform) {
    	global $DB, $CFG;

        // Section header title according to language file.
        $mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));

		// output the Printer table
		$columns = array("id", "printername", "printerid", "usable", "printerip", "subnet");
		$headings = array();
		$string = array(
			    	'id' => get_string('id','block_programming_printer'),
			    	'printername' => get_string('printername','block_programming_printer'),
			    	'printerid' => get_string('printerid','block_programming_printer'),
			    	'usable' => get_string('usable','block_programming_printer'),
			    	'printerip' => get_string('printerip','block_programming_printer'),
			    	'subnet' => get_string('subnet','block_programming_printer')
		    	);
		// 表头
		foreach ($columns as $column) {
		    $headings[$column] = $string[$column];
		}
		// 读取打印机信息，并生成表格
		$select = '(usable = 1 OR usable = 0)';
		$printers = $DB->get_records_select('block_programming_printer', $select, null, $DB->sql_order_by_text('id'));
		if (!$printers) {
			$mform->addElement('static', 'noprinter', 'Printers',get_string('nousableprinter','block_programming_printer'));

		} else {
		    $table = new html_table();
		    $table->head = $headings;
		    $table->align = array('left', 'left', 'left', 'left', 'left', 'left');
		    $table->width = "95%";
		    foreach ($printers as $printer) {
		    	// var_dump($printer);
		    	$table->data[] = array(
		    				$printer->id,
		    				$printer->printername,
		    				$printer->printerid,
		    				$printer->usable,
		    				$printer->printerip,
		    				$printer->subnet
		    				);

		    	// var_dump($table);
		    }
	    	$c = html_writer::table($table);
	    	// var_dump($c);
	    	$mform->addElement('static', 'printers', 'Printers',$c);
		}

		// 添加/编辑打印机信息
		if (has_any_capability(array('block/programming_printer:manageanyprinters'), $this->block->context)) {
			$mform->addElement('static', 'noprinter', '',
				'<a href="' . $CFG->wwwroot . '/blocks/programming_printer/manageprinters.php?courseid=' . $this->page->course->id . '">' .
				get_string('printersaddedit', 'block_programming_printer') . '</a>');
		}
    }
}
