<?php
/**
 * 打印机信息管理
 *
 * @package   block_programming_printer
 * 
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir .'/simplepie/moodle_simplepie.php');

class printer_edit_form extends moodleform {
    protected $isadding;
    protected $title = '';
    protected $description = '';

    function __construct($actionurl, $isadding, $caneditshared) {
        $this->isadding = $isadding;
        parent::moodleform($actionurl);
    }

    function definition() {
        $mform =& $this->_form;

        // Then show the fields about where this block appears.
        $mform->addElement('header', 'editprinterheader', get_string('printer', 'block_programming_printer'));
        // printername
        $mform->addElement('text', 'printername', get_string('printername','block_programming_printer'), array('size' => 60));
        $mform->setType('printername', PARAM_NOTAGS);
        $mform->addRule('printername', null, 'required');
        // printerid
        $mform->addElement('text', 'printerid', get_string('printerid','block_programming_printer'), array('size' => 60));
        $mform->setType('printerid', PARAM_ALPHANUMEXT); // number letter - _
        $mform->addRule('printerid', null, 'required');
        // printerip   
        $mform->addElement('text', 'printerip', get_string('printerip','block_programming_printer'), array('size' => 48));
        $mform->setType('printerip', PARAM_NOTAGS); // 10.2.1.232:631
        $mform->addRule('printerip', null, 'required');
        // subnet
        $mform->addElement('text', 'subnet', get_string('subnet','block_programming_printer'), array('size' => 48));
        $mform->setType('subnet', PARAM_NOTAGS); // e.g:10.2.1.0/23
        $mform->addRule('subnet', null, 'required');
        // usable
        $mform->addElement('selectyesno', 'usable', get_string('usable', 'block_programming_printer'));
        $mform->setDefault('usable', 0);

        $submitlabal = null; // Default
        if ($this->isadding) {
            $submitlabal = get_string('addnewprinter', 'block_programming_printer');
        }
        $this->add_action_buttons(true, $submitlabal);
    }
}

$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
$courseid = optional_param('courseid', 0, PARAM_INT);
$pid = optional_param('pid', 0, PARAM_INT); // 0 mean create new.

// 判断是否登录与是否为访客
require_login(0,false);
if (isguestuser()) {
    // Login as real user!
    $SESSION->wantsurl = (string)new moodle_url('/index.php');
    redirect(get_login_url());
}

if ($courseid == SITEID) {
    $courseid = 0;
}
if ($courseid) {
    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
    $PAGE->set_course($course);
    $context = $PAGE->context;
} else {
    $context = context_system::instance();
    $PAGE->set_context($context);
}

// 判断是否拥有修改权限
$manageprinters = has_capability('block/programming_printer:manageanyprinters', $context);


$urlparams = array('pid' => $pid);
if ($courseid) {
    $urlparams['courseid'] = $courseid;
}
if ($returnurl) {
    $urlparams['returnurl'] = $returnurl;
}
$manageprintersurl = new moodle_url('/blocks/programming_printer/manageprinters.php', $urlparams);

$PAGE->set_url('/blocks/programming_printer/editprinter.php', $urlparams);
$PAGE->set_pagelayout('admin');

// 判断是添加还是修改
if ($pid) {
    $isadding = false;
    $printerrecord = $DB->get_record('block_programming_printer', array('id' => $pid), '*', MUST_EXIST);
} else {
    $isadding = true;
    $printerrecord = new stdClass;
}

$mform = new printer_edit_form($PAGE->url, $isadding, $manageprinters);
$mform->set_data($printerrecord);

// 是否取消
if ($mform->is_cancelled()) {
    redirect($manageprintersurl);

} else if ($data = $mform->get_data()) { // 提交
    if ($isadding) {
        $DB->insert_record('block_programming_printer', $data, $returnid=true, $primarykey='id');
    } else {
        $data->id = $pid;
        $DB->update_record('block_programming_printer', $data);
    }

    redirect($manageprintersurl);

} else { // 
    if ($isadding) {
        $strtitle = get_string('addnewprinter', 'block_programming_printer');
    } else {
        $strtitle = get_string('editaprinter', 'block_programming_printer');
    }

    $PAGE->set_title($strtitle);
    $PAGE->set_heading($strtitle);

    $PAGE->navbar->add(get_string('blocks'));
    $PAGE->navbar->add(get_string('pluginname', 'block_programming_printer'));
    $PAGE->navbar->add(get_string('manageprinters', 'block_programming_printer'), $manageprintersurl );
    $PAGE->navbar->add($strtitle);

    echo $OUTPUT->header();
    echo $OUTPUT->heading($strtitle, 2);

    $mform->display();

    echo $OUTPUT->footer();
}

