<?php
/**
 * 管理打印机
 * @package   block_programming_printer
 * 
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/tablelib.php');

// 是否登录
require_login();

$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
$courseid = optional_param('courseid', 0, PARAM_INT);
$deleteprinterid = optional_param('deleteprinterid', 0, PARAM_INT);

if ($courseid == SITEID) {
    $courseid = 0;
}
if ($courseid) {
    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
    $PAGE->set_course($course);
    $context = $PAGE->context;
} else {
    $context = context_system::instance();
    $PAGE->set_context($context);
}

$manageprinters = has_capability('block/programming_printer:manageanyprinters', $context);
/**
 * TODO 添加权限控制
 */
if (!$manageprinters) {
    require_capability('block/programming_printer:manageownprinters', $context);
}

$urlparams = array();
$extraparams = '';
if ($courseid) {
    $urlparams['courseid'] = $courseid;
    $extraparams = '&courseid=' . $courseid;
}
if ($returnurl) {
    $urlparams['returnurl'] = $returnurl;
    $extraparams = '&returnurl=' . $returnurl;
}
$baseurl = new moodle_url('/blocks/programming_printer/manageprinters.php', $urlparams);
$PAGE->set_url($baseurl);

// 删除打印机信息
if ($deleteprinterid && confirm_sesskey()) {
    $DB->delete_records('block_programming_printer', array('id'=>$deleteprinterid));
    redirect($PAGE->url, get_string('printerdeleted', 'block_programming_printer'));
}

// Display the list of printers.
if ($manageprinters) {
    $select = '(usable = 1 OR usable = 0)';
}

$strmanage = get_string('manageprinters', 'block_programming_printer');

$PAGE->set_pagelayout('standard');
$PAGE->set_title($strmanage);
$PAGE->set_heading($strmanage);

$manageprintersurl = new moodle_url('/blocks/programming_printer/manageprinters.php', $urlparams);
$PAGE->navbar->add(get_string('blocks'));
$PAGE->navbar->add(get_string('pluginname', 'block_programming_printer'));
$PAGE->navbar->add(get_string('manageprinters', 'block_programming_printer'), $manageprintersurl);
echo $OUTPUT->header();

// 获取所有打印机信息，并生成表格
$printers = $DB->get_records_select('block_programming_printer', $select, null, $DB->sql_order_by_text('id'));

$table = new flexible_table('display-printers');

$table->define_columns(array('id','printername','printerid','usable','printerip','subnet','actions'));
$table->define_headers(array(
    get_string('id','block_programming_printer'),
    get_string('printername','block_programming_printer'),
    get_string('printerid','block_programming_printer'),
    get_string('usable','block_programming_printer'),
    get_string('printerip','block_programming_printer'),
    get_string('subnet','block_programming_printer'),
    get_string('actions', 'moodle')
    ));
$table->define_baseurl($baseurl);


$table->set_attribute('cellspacing', '0');
$table->set_attribute('id', 'printers');
$table->set_attribute('class', 'generaltable generalbox');

$table->column_class('id','id');
$table->column_class('printername','printername');
$table->column_class('printerid','printerid');
$table->column_class('usable','usable');
$table->column_class('printerip','printerip');
$table->column_class('subnet','subnet');
$table->column_class('actions', 'actions');
$table->setup();

// 为每个打印机添加操作按钮，加到表格中
foreach($printers as $printer) {

    $editurl = new moodle_url('/blocks/programming_printer/editprinter.php?pid=' . $printer->id . $extraparams);
    $editaction = $OUTPUT->action_icon($editurl, new pix_icon('t/edit', get_string('edit')));

    $deleteurl = new moodle_url('/blocks/programming_printer/manageprinters.php?deleteprinterid=' . $printer->id . '&sesskey=' . sesskey() . $extraparams);
    $deleteicon = new pix_icon('t/delete', get_string('delete'));
    $deleteaction = $OUTPUT->action_icon($deleteurl, $deleteicon, new confirm_action(get_string('deleteprinterconfirm', 'block_programming_printer')));

    $printericons = $editaction . ' ' . $deleteaction;


    $table->add_data(array($printer->id, 
        $printer->printername,
        $printer->printerid,
        $printer->usable,
        $printer->printerip,
        $printer->subnet,
        $printericons));

}

$table->print_html();

$url = $CFG->wwwroot . '/blocks/programming_printer/editprinter.php?' . substr($extraparams, 1);
echo '<div class="actionbuttons">' . $OUTPUT->single_button($url, get_string('addnewprinter', 'block_programming_printer'), 'get') . '</div>';

if ($returnurl) {
    echo '<div class="backlink">' . html_writer::link($returnurl, get_string('back')) . '</div>';
}

echo $OUTPUT->footer();
